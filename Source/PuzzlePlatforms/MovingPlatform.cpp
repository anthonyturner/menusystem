// Fill out your copyright notice in the Description page of Project Settings.
#include "MovingPlatform.h"
#include "MovingPlatform.h"
//#include "engine.h"

AMovingPlatform::AMovingPlatform() {

	PrimaryActorTick.bCanEverTick = true;
	SetMobility(EComponentMobility::Movable);
	Speed = 5.f;
}



void AMovingPlatform::BeginPlay(){

	Super::BeginPlay();

	if (HasAuthority()) {
	
		SetReplicates(true);
		SetReplicateMovement(true);
	}

	GlobalStartLocation = GetActorLocation();
	GlobalTargetLocation = GetTransform().TransformPosition(TargetLocation);

}

void AMovingPlatform::Tick(float DeltaTime) {

	Super::Tick(DeltaTime);

	if (ActiveTriggers > 0) {

		MovePlatform(DeltaTime);
	}

	
}

void AMovingPlatform::MovePlatform(float DeltaTime) {

	if (HasAuthority()) {

		FVector ActorLocation = GetActorLocation();
		float JourneyLength = (GlobalTargetLocation - GlobalStartLocation).Size();
		float JourneyTravelled = (ActorLocation - GlobalStartLocation).Size();

		//Check distance to target
		if (JourneyTravelled >= JourneyLength) {
			//Start location is now the reached destination location
			FVector Swap = GlobalStartLocation;
			GlobalStartLocation = GlobalTargetLocation;
			GlobalTargetLocation = Swap;
		}
		//Calculate Direction
		FVector Direction = (GlobalTargetLocation - ActorLocation).GetSafeNormal();
		ActorLocation += (Speed * DeltaTime * Direction);
		SetActorLocation(ActorLocation);
	}
}

void AMovingPlatform::AddActiveTrigger(){

	ActiveTriggers++;
}

void AMovingPlatform::RemoveActiveTrigger(){

	if( ActiveTriggers > 0)	ActiveTriggers--;
}
